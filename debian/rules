#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

PY3VERS=$(shell py3versions -vr)

DEBIAN_DIR := $(shell echo ${MAKEFILE_LIST} | awk '{print $$1}' | xargs dirname )

%:
	dh $@ --with python3,autoreconf

override_dh_auto_clean:
	( cd libconcord/ && [ ! -f Makefile ] || $(MAKE) clean )
	rm -f libconcord/Makefile libconcord/config.h \
		libconcord/config.status libconcord/config.log \
		libconcord/libconcord.rules \
		libconcord/libtool libconcord/stamp-h1
	rm -rf libconcord/bindings/python/build
	( cd concordance/ && [ ! -f Makefile ] || $(MAKE) clean )
	rm -f concordance/Makefile concordance/config.h \
		concordance/config.status concordance/config.log \
		concordance/libtool concordance/stamp-h1
	find . -name *\.py[co] -exec rm {} \;
	rm -f build-ext-* install-ext-*
	rm -f libconcord/libconcord-usbnet.rules
	dh_auto_clean

override_dh_auto_configure:
	( cd libconcord/ && ./configure --prefix=/usr --sysconfdir=/etc )
	( cd concordance/ && ./configure --prefix=/usr $(shell DEB_CFLAGS_MAINT_APPEND=-L../libconcord/.libs DEB_CPPFLAGS_MAINT_APPEND=-I../libconcord dpkg-buildflags --export=configure))

build-ext-%:
	( cd libconcord/bindings/python && python$* setup.py build )
	touch $@

override_dh_auto_build: $(PY3VERS:%=build-ext-%)
	( cd libconcord/ && $(MAKE) )
	( cd libconcord/ && $(MAKE) udev )
	( cd concordance/ && $(MAKE) )

install-ext3-%:
	( cd libconcord/bindings/python && python$* setup.py install --root=$(CURDIR)/debian/python3-libconcord )

override_dh_auto_install: $(PY3VERS:%=install-ext3-%)
	( cd libconcord/ && $(MAKE) DESTDIR=$(CURDIR)/debian/tmp install )
	( cd libconcord/ && $(MAKE) DESTDIR=$(CURDIR)/debian/tmp install_udev )
	( cd concordance/ && $(MAKE) DESTDIR=$(CURDIR)/debian/tmp install )

override_dh_install:
	find $(CURDIR)/debian -name *.la -delete
	find $(CURDIR)/debian -name *.a -delete
	dh_install

override_dh_missing:
	dh_missing --fail-missing
